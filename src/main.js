import {
  createBrowserRouter,
  RouterProvider
} from "react-router-dom";
import React from 'react';
import { createRoot } from 'react-dom/client';

const router = createBrowserRouter([
  {
    path: "/",
    element: <div>Hello world!</div>,
  },
]);

createRoot(document.querySelector('#app')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
