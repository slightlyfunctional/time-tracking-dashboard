import styled from 'styled-components';

const Container = styled.div`
`;

export default function Dashboard () {
  return ( 
    <>
      <section>
        <p>Report for</p>
        <h1>Jeremy Robson</h1>
        <nav>
          Daily
          Weekly
          Monthly
        </nav>
      </section>
    </>
  );
}
